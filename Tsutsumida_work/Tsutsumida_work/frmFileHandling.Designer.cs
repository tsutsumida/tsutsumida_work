﻿namespace Tsutsumida_work {
	partial class frmFileHandling {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.btnFileOpen = new System.Windows.Forms.Button();
			this.tboxText = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
			// 
			// btnFileOpen
			// 
			this.btnFileOpen.Location = new System.Drawing.Point(268, 12);
			this.btnFileOpen.Name = "btnFileOpen";
			this.btnFileOpen.Size = new System.Drawing.Size(75, 23);
			this.btnFileOpen.TabIndex = 0;
			this.btnFileOpen.Text = "ファイルを開く";
			this.btnFileOpen.UseVisualStyleBackColor = true;
			this.btnFileOpen.Click += new System.EventHandler(this.btnFileOpen_Click);
			// 
			// tboxText
			// 
			this.tboxText.Location = new System.Drawing.Point(13, 46);
			this.tboxText.Multiline = true;
			this.tboxText.Name = "tboxText";
			this.tboxText.Size = new System.Drawing.Size(330, 322);
			this.tboxText.TabIndex = 1;
			// 
			// frmFileHandling
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(355, 380);
			this.Controls.Add(this.tboxText);
			this.Controls.Add(this.btnFileOpen);
			this.Name = "frmFileHandling";
			this.Text = "frmFileHandling";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button btnFileOpen;
		private System.Windows.Forms.TextBox tboxText;
	}
}