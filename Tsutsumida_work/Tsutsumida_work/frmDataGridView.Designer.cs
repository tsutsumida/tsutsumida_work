﻿namespace DataGridViewHandling {
	partial class frmDataGridView {
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent() {
			this.dgvA = new System.Windows.Forms.DataGridView();
			this.dgvB = new System.Windows.Forms.DataGridView();
			this.dgvJoin = new System.Windows.Forms.DataGridView();
			this.lblTableA = new System.Windows.Forms.Label();
			this.lblTableB = new System.Windows.Forms.Label();
			this.lblTableJoin = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dgvA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvJoin)).BeginInit();
			this.SuspendLayout();
			// 
			// dgvA
			// 
			this.dgvA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvA.Location = new System.Drawing.Point(12, 12);
			this.dgvA.Name = "dgvA";
			this.dgvA.RowTemplate.Height = 21;
			this.dgvA.Size = new System.Drawing.Size(348, 150);
			this.dgvA.TabIndex = 0;
			// 
			// dgvB
			// 
			this.dgvB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvB.Location = new System.Drawing.Point(12, 202);
			this.dgvB.Name = "dgvB";
			this.dgvB.RowTemplate.Height = 21;
			this.dgvB.Size = new System.Drawing.Size(348, 150);
			this.dgvB.TabIndex = 1;
			// 
			// dgvJoin
			// 
			this.dgvJoin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvJoin.Location = new System.Drawing.Point(12, 392);
			this.dgvJoin.Name = "dgvJoin";
			this.dgvJoin.RowTemplate.Height = 21;
			this.dgvJoin.Size = new System.Drawing.Size(348, 150);
			this.dgvJoin.TabIndex = 2;
			// 
			// lblTableA
			// 
			this.lblTableA.AutoSize = true;
			this.lblTableA.Location = new System.Drawing.Point(133, 165);
			this.lblTableA.Name = "lblTableA";
			this.lblTableA.Size = new System.Drawing.Size(75, 12);
			this.lblTableA.TabIndex = 3;
			this.lblTableA.Text = "DataTable : A";
			// 
			// lblTableB
			// 
			this.lblTableB.AutoSize = true;
			this.lblTableB.Location = new System.Drawing.Point(133, 358);
			this.lblTableB.Name = "lblTableB";
			this.lblTableB.Size = new System.Drawing.Size(75, 12);
			this.lblTableB.TabIndex = 4;
			this.lblTableB.Text = "DataTable : B";
			// 
			// lblTableJoin
			// 
			this.lblTableJoin.AutoSize = true;
			this.lblTableJoin.Location = new System.Drawing.Point(120, 551);
			this.lblTableJoin.Name = "lblTableJoin";
			this.lblTableJoin.Size = new System.Drawing.Size(101, 12);
			this.lblTableJoin.TabIndex = 5;
			this.lblTableJoin.Text = "DataTable : A + B ";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(285, 546);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 6;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// frmDataGridView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(375, 579);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.lblTableJoin);
			this.Controls.Add(this.lblTableB);
			this.Controls.Add(this.lblTableA);
			this.Controls.Add(this.dgvJoin);
			this.Controls.Add(this.dgvB);
			this.Controls.Add(this.dgvA);
			this.Name = "frmDataGridView";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.OnLoad);
			((System.ComponentModel.ISupportInitialize)(this.dgvA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvJoin)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView dgvA;
		private System.Windows.Forms.DataGridView dgvB;
		private System.Windows.Forms.DataGridView dgvJoin;
		private System.Windows.Forms.Label lblTableA;
		private System.Windows.Forms.Label lblTableB;
		private System.Windows.Forms.Label lblTableJoin;
		private System.Windows.Forms.Button button1;
	}
}

