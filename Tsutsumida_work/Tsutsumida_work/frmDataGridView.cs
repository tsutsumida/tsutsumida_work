﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataGridViewHandling {
	public partial class frmDataGridView : Form {
		private DataTable _tableA;
		private DataTable _tableB;
		private DataTable _tableJoin;

		public frmDataGridView() {
			InitializeComponent();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Form1_Load(object sender, EventArgs e) {

		}
		/// <summary>
		/// DataTable : AとDataTable : Bを結合する。RelationキーはIDとする。
		/// 結合後のテーブルを、DataTable : A + Bに表示させる。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void button1_Click(object sender, EventArgs e) {

			try {
				DataTable dt = _tableA.Copy();
				dt.Merge(_tableB);
				//DataRow[] rows = dt.Select().Where(c => c["Check"].Equals(false)).ToArray();
				DataRow[] rows = dt.Select("Check=false");

				_tableJoin = rows.CopyToDataTable();
				this.dgvJoin.DataSource = _tableJoin;
				this.dgvJoin.Show();
			} catch (Exception ex) {

			}
		}

		/// <summary>
		/// TableAを作成するメソッド
		/// checkboxカラム, IDカラム
		/// </summary>
		/// <returns></returns>
		private DataTable CreateTableA(int size) {
			DataTable dt = new DataTable("Table A");
			DataColumn col = new DataColumn();

			// ■カラムの追加
			// checkboxカラムの追加
			col.DataType = typeof(System.Boolean);
			col.ColumnName = "Check";
			col.DefaultValue = false;
			dt.Columns.Add(col);

			// IDカラムの追加
			/*
			col = new DataColumn();
			col.DataType = typeof(System.Int32);
			col.ColumnName = "ID";
			dt.Columns.Add(col);
			*/

			// ■レコードの追加
			for (int i = 0; i < size; i++) {
				DataRow row = dt.NewRow();
				row["Check"]=true;
				//row["ID"] = i+1;
				dt.Rows.Add(row);
			}

			return dt;
		}

		/// <summary>
		/// TableBを作成するメソッド
		/// IDカラム、名前カラム
		/// </summary>
		/// <returns></returns>
		private DataTable CreateTableB() {
			DataTable dt = new DataTable("Table B");
			DataColumn col = new DataColumn();

			// ■カラムの追加
			// IDカラムの追加
			col.DataType = typeof(System.Int32);
			col.ColumnName = "ID";
			dt.Columns.Add(col);

			// 名前カラムの追加
			col = new DataColumn();
			col.DataType = typeof(System.String);
			col.ColumnName = "Name";
			dt.Columns.Add(col);

			// ■レコードの追加
			DataRow row = dt.NewRow();
			row["ID"] = 1;
			row["Name"] = "Alpha";
			dt.Rows.Add(row);

			row = dt.NewRow();
			row["ID"] = 2;
			row["Name"] = "Bravo";
			dt.Rows.Add(row);

			row = dt.NewRow();
			row["ID"] = 3;
			row["Name"] = "Charly";
			dt.Rows.Add(row);

			row = dt.NewRow();
			row["ID"] = 4;
			row["Name"] = "Delta";　
			dt.Rows.Add(row);

			return dt;
		}

		/// <summary>
		/// フォームが読み込まれた時に発生するイベント
		/// Table A, Table Bにサンプルデータを入れて出力する。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLoad(object sender, EventArgs e) {
			try {
				_tableB = CreateTableB();
				int size = _tableB.Rows.Count;
				_tableA = CreateTableA(size);

				dgvB.DataSource = _tableB;
				dgvA.DataSource = _tableA;

				dgvA.Show();
				dgvB.Show();
			} catch (Exception ex) {

			}
		}
	}
}
