﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace Tsutsumida_work {
	public partial class frmFileHandling : Form {
		public frmFileHandling() {
			InitializeComponent();
		}

		/// <summary>
		/// ファイルを開くボタンをクリックした時のイベント
		/// openFileDialogのダイアログを表示させる。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnFileOpen_Click(object sender, EventArgs e) {
			//openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
			openFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
			openFileDialog1.ShowDialog();
		}

		/// <summary>
		/// ファイルを開くダイアログ
		/// openFileDialogにて、OKボタンが押された場合の処理
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void openFileDialog1_FileOk(object sender, CancelEventArgs e) {
			Stream stream = openFileDialog1.OpenFile();

			if (stream != null) {
				Encoding enc = Encoding.GetEncoding("shift-jis");
				using (StreamReader sr = new StreamReader(stream,enc)) {
					tboxText.Text = sr.ReadToEnd();
				}
			}
		}


	}
}
