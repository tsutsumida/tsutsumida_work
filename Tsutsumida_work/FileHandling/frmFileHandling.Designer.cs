﻿namespace FileHandling {
	partial class frmFileHandling {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.btnFileOpen = new System.Windows.Forms.Button();
			this.tboxText = new System.Windows.Forms.TextBox();
			this.btnFileWrite = new System.Windows.Forms.Button();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.btnGrep = new System.Windows.Forms.Button();
			this.tboxGrep = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.lblGrep = new System.Windows.Forms.Label();
			this.tableLayoutPanel1.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
			// 
			// btnFileOpen
			// 
			this.btnFileOpen.Location = new System.Drawing.Point(190, 12);
			this.btnFileOpen.Name = "btnFileOpen";
			this.btnFileOpen.Size = new System.Drawing.Size(75, 23);
			this.btnFileOpen.TabIndex = 0;
			this.btnFileOpen.Text = "ファイルを開く";
			this.btnFileOpen.UseVisualStyleBackColor = true;
			this.btnFileOpen.Click += new System.EventHandler(this.btnFileOpen_Click);
			// 
			// tboxText
			// 
			this.tboxText.Location = new System.Drawing.Point(13, 46);
			this.tboxText.Multiline = true;
			this.tboxText.Name = "tboxText";
			this.tboxText.Size = new System.Drawing.Size(330, 322);
			this.tboxText.TabIndex = 1;
			// 
			// btnFileWrite
			// 
			this.btnFileWrite.Location = new System.Drawing.Point(268, 12);
			this.btnFileWrite.Name = "btnFileWrite";
			this.btnFileWrite.Size = new System.Drawing.Size(75, 23);
			this.btnFileWrite.TabIndex = 2;
			this.btnFileWrite.Text = "保存する";
			this.btnFileWrite.UseVisualStyleBackColor = true;
			this.btnFileWrite.Click += new System.EventHandler(this.btnFileWrite_Click);
			// 
			// saveFileDialog1
			// 
			this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.56634F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.43365F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
			this.tableLayoutPanel1.Controls.Add(this.btnGrep, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.tboxGrep, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 374);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(330, 31);
			this.tableLayoutPanel1.TabIndex = 3;
			// 
			// btnGrep
			// 
			this.btnGrep.Location = new System.Drawing.Point(251, 3);
			this.btnGrep.Name = "btnGrep";
			this.btnGrep.Size = new System.Drawing.Size(75, 23);
			this.btnGrep.TabIndex = 1;
			this.btnGrep.Text = "Grep";
			this.btnGrep.UseVisualStyleBackColor = true;
			this.btnGrep.Click += new System.EventHandler(this.btnGrep_Click);
			// 
			// tboxGrep
			// 
			this.tboxGrep.Location = new System.Drawing.Point(66, 3);
			this.tboxGrep.Name = "tboxGrep";
			this.tboxGrep.Size = new System.Drawing.Size(179, 19);
			this.tboxGrep.TabIndex = 0;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.lblGrep);
			this.panel1.Location = new System.Drawing.Point(3, 3);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(57, 25);
			this.panel1.TabIndex = 0;
			// 
			// lblGrep
			// 
			this.lblGrep.AutoSize = true;
			this.lblGrep.Location = new System.Drawing.Point(3, 7);
			this.lblGrep.Name = "lblGrep";
			this.lblGrep.Size = new System.Drawing.Size(53, 12);
			this.lblGrep.TabIndex = 0;
			this.lblGrep.Text = "Grep文字";
			// 
			// frmFileHandling
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(355, 417);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.btnFileWrite);
			this.Controls.Add(this.tboxText);
			this.Controls.Add(this.btnFileOpen);
			this.Name = "frmFileHandling";
			this.Text = "frmFileHandling";
			this.Load += new System.EventHandler(this.frmFileHandling_Load);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button btnFileOpen;
		private System.Windows.Forms.TextBox tboxText;
		private System.Windows.Forms.Button btnFileWrite;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Button btnGrep;
		private System.Windows.Forms.TextBox tboxGrep;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label lblGrep;
	}
}