﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace  FileHandling{
	public partial class frmFileHandling : Form {
		public frmFileHandling() {
			InitializeComponent();
		}

		private void SettingTabIndex() {
			this.btnFileOpen.TabIndex = 10;
			this.btnFileWrite.TabIndex = 20;

			this.tboxText.TabIndex = 100;
			this.tableLayoutPanel1.TabIndex = 105;
			this.panel1.TabIndex = 107;
			this.lblGrep.TabIndex = 110;
			this.tboxGrep.TabIndex = 120;
			this.btnGrep.TabIndex = 130;
		}

		/// <summary>
		/// ファイルを開くボタンをクリックした時のイベント
		/// openFileDialogのダイアログを表示させる。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnFileOpen_Click(object sender, EventArgs e) {
			openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
			//openFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
			openFileDialog1.ShowDialog();
		}

		/// <summary>
		/// ファイルを開くダイアログ
		/// openFileDialogにて、OKボタンが押された場合の処理
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void openFileDialog1_FileOk(object sender, CancelEventArgs e) {
			/*
			Stream stream = openFileDialog1.OpenFile();

			if (stream != null) {
				Encoding enc = Encoding.GetEncoding("shift-jis");
				using (StreamReader sr = new StreamReader(stream,enc)) {
					tboxText.Text = sr.ReadToEnd();
				}
			}
			*/
			string filepath = openFileDialog1.FileName;
			tboxText.Text = File.ReadAllText(filepath);
		}

		/// <summary>
		/// 保存ボタンがクリックされた場合の処理
		/// 名前を付けて保存ダイアログを表示させる。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnFileWrite_Click(object sender, EventArgs e) {
			saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
			saveFileDialog1.ShowDialog();
		}

		/// <summary>
		/// 名前を付けて保存ダイアログでOKが押された場合の処理
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void saveFileDialog1_FileOk(object sender, CancelEventArgs e) {
			Encoding enc = Encoding.GetEncoding("shift-jis");
			string filepath = saveFileDialog1.FileName;
			/*
			using (StreamWriter sw = new StreamWriter(filepath, false, enc)) {
				sw.Write(tboxText.Text);
			}
			*/
			File.WriteAllText(filepath, tboxText.Text);
		}

		/// <summary>
		/// Grepするためのボタン
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnGrep_Click(object sender, EventArgs e) {
			string filepath = this.openFileDialog1.FileName;
			string grep = this.tboxGrep.Text;

			if (!File.Exists(filepath) || string.IsNullOrEmpty(grep)) {
				return ;
			}

			string[] text = File.ReadAllLines(filepath).Where(c => c.Contains(grep)).ToArray();
			StringBuilder sBuild = new StringBuilder();
			foreach (string m in text) {
				sBuild.Append(m + "\r\n");
			}
			this.tboxText.Text = sBuild.ToString();
		}

		/// <summary>
		/// frmFileHandlingが出力される際に呼び出されるイベント
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmFileHandling_Load(object sender, EventArgs e) {
			SettingTabIndex();
		}
	}
}
