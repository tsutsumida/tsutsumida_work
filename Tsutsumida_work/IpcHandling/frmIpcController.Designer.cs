﻿namespace IpcHandling {
	partial class frmIpcController {
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent() {
			this.btnCallServer = new System.Windows.Forms.Button();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.tboxSrvCount = new System.Windows.Forms.TextBox();
			this.lblSrvCaptoin = new System.Windows.Forms.Label();
			this.btnCallClient = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tboxCltCount = new System.Windows.Forms.TextBox();
			this.lblCltCaption = new System.Windows.Forms.Label();
			this.tableLayoutPanel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnCallServer
			// 
			this.btnCallServer.Location = new System.Drawing.Point(3, 3);
			this.btnCallServer.Name = "btnCallServer";
			this.btnCallServer.Size = new System.Drawing.Size(112, 23);
			this.btnCallServer.TabIndex = 0;
			this.btnCallServer.Text = "Server起動";
			this.btnCallServer.UseVisualStyleBackColor = true;
			this.btnCallServer.Click += new System.EventHandler(this.btnCallServer_Click);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.btnCallClient, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.btnCallServer, 0, 0);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(236, 62);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tboxSrvCount);
			this.panel2.Controls.Add(this.lblSrvCaptoin);
			this.panel2.Location = new System.Drawing.Point(3, 34);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(112, 23);
			this.panel2.TabIndex = 4;
			// 
			// tboxSrvCount
			// 
			this.tboxSrvCount.Location = new System.Drawing.Point(69, 1);
			this.tboxSrvCount.Name = "tboxSrvCount";
			this.tboxSrvCount.Size = new System.Drawing.Size(43, 19);
			this.tboxSrvCount.TabIndex = 2;
			this.tboxSrvCount.Text = "0";
			this.tboxSrvCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tboxSrvCount.TextChanged += new System.EventHandler(this.tboxSrvCount_TextChanged);
			// 
			// lblSrvCaptoin
			// 
			this.lblSrvCaptoin.AutoSize = true;
			this.lblSrvCaptoin.Location = new System.Drawing.Point(3, 5);
			this.lblSrvCaptoin.Name = "lblSrvCaptoin";
			this.lblSrvCaptoin.Size = new System.Drawing.Size(70, 12);
			this.lblSrvCaptoin.TabIndex = 2;
			this.lblSrvCaptoin.Text = "起動SRV数：";
			// 
			// btnCallClient
			// 
			this.btnCallClient.Location = new System.Drawing.Point(121, 3);
			this.btnCallClient.Name = "btnCallClient";
			this.btnCallClient.Size = new System.Drawing.Size(112, 23);
			this.btnCallClient.TabIndex = 1;
			this.btnCallClient.Text = "Client起動";
			this.btnCallClient.UseVisualStyleBackColor = true;
			this.btnCallClient.Click += new System.EventHandler(this.btnCallClient_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.tboxCltCount);
			this.panel1.Controls.Add(this.lblCltCaption);
			this.panel1.Location = new System.Drawing.Point(121, 34);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(112, 23);
			this.panel1.TabIndex = 3;
			// 
			// tboxCltCount
			// 
			this.tboxCltCount.Location = new System.Drawing.Point(69, 1);
			this.tboxCltCount.Name = "tboxCltCount";
			this.tboxCltCount.Size = new System.Drawing.Size(43, 19);
			this.tboxCltCount.TabIndex = 3;
			this.tboxCltCount.Text = "0";
			this.tboxCltCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.tboxCltCount.TextChanged += new System.EventHandler(this.tboxCltCount_TextChanged);
			// 
			// lblCltCaption
			// 
			this.lblCltCaption.AutoSize = true;
			this.lblCltCaption.Location = new System.Drawing.Point(3, 5);
			this.lblCltCaption.Name = "lblCltCaption";
			this.lblCltCaption.Size = new System.Drawing.Size(68, 12);
			this.lblCltCaption.TabIndex = 2;
			this.lblCltCaption.Text = "起動CLT数：";
			// 
			// frmIpcController
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(263, 84);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "frmIpcController";
			this.Text = "IPCコントローラー";
			this.Load += new System.EventHandler(this.frmIpcController_Load);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnCallServer;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label lblSrvCaptoin;
		private System.Windows.Forms.Button btnCallClient;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label lblCltCaption;
		private System.Windows.Forms.TextBox tboxSrvCount;
		private System.Windows.Forms.TextBox tboxCltCount;
	}
}

