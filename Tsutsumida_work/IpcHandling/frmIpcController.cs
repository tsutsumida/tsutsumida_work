﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IpcHandling {

	//【課題】
	// frmIpcControler
	// 1. srv,cltの生成できる数を制御する。
	// 2. 入力した値の数だけcltを同時生成する
	// 3. それぞれのcltが独立して動き、複数のcltからsrvへメッセージを送れるようにする。
	// 4. srvが受け取るメッセージのフォーマットを変更し、メッセージを追記するようにする。

	/// <summary>
	/// frmIpcServerとfrmIpcClientの生成をコントロールするクラス
	/// </summary>
	public partial class frmIpcController : Form {

		/// <summary>
		/// IPC通信用クラス（クライアント）
		/// </summary>
		private frmIpcClient _clt;

		/// <summary>
		/// IPC通信用クラス（サーバー）
		/// </summary>
		private frmIpcServer _srv;

		/// <summary>
		/// SRVの数をカウントする
		/// 課題：プロセスからSRVの数を取得するように変更
		/// </summary>
		private int _sCount = 0;

		/// <summary>
		/// CLTの数をカウントする
		/// 課題：プロセスからCLTの数を取得するように変更
		/// </summary>
		private int _cCount = 0;

		/// <summary>
		/// コンストラクター
		/// </summary>
		public frmIpcController() {
			InitializeComponent();
		}

		/// <summary>
		/// TabIndex番号を整理する関数
		/// </summary>
		private void SettingTabIndex() {
			this.tableLayoutPanel1.TabIndex = 10;
			this.btnCallServer.TabIndex = 20;
			this.btnCallClient.TabIndex = 30;
			this.lblSrvCaptoin.TabIndex = 40;
			this.tboxSrvCount.TabIndex = 50;
			this.lblCltCaption.TabIndex = 60;
			this.tboxCltCount.TabIndex = 70;
		}

		/// <summary>
		/// SRVカウントが変更された時のイベント
		/// カウントの数を調整する。
		/// min 0 : max 1
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tboxSrvCount_TextChanged(object sender, EventArgs e) {
			// 1. SRV起動ボタンが押された時 0 -> 1に変更後　SRV起動ボタンを使用不可にする
			// 2. 直接変更された時、使用可能 = {0,1},
			//    使用可能以外のとき value < 0 の時は、0にする。　1 < valueの時は1にする
		}

		/// <summary>
		/// CLTカウントが変更された時のイベント
		/// カウントの数を調整する。
		/// min 0 : max 31
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tboxCltCount_TextChanged(object sender, EventArgs e) {
			// 1. SRV起動ボタンが押された時 0 -> 1に変更後　SRV起動ボタンを使用不可にする
			// 2. 直接変更された時、使用可能 = {0,...,31},
			//    使用可能以外のとき value < 0 の時は、0にする。　31 < valueの時は255にする
		}

		/// <summary>
		/// IPC通信用のフォーム（SRV）を出力する
		/// 0か1つだけとする。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnCallServer_Click(object sender, EventArgs e) {
			// プロセスからSRVの起動数をカウントして起動を制限する。（課題にする？）
			// IpcServerを起動する
			// SRVを起動した場合起動ボタンをdisableにする。

			if (_sCount == 0) {
				_srv.Show();
			}
		}

		/// <summary>
		/// IPC通信用のフォーム（CLT）を出力する
		/// 0から31の間で出力する。（予定。リソースの消費量によっては変更する。）
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnCallClient_Click(object sender, EventArgs e) {
			// プロセスからCLTの起動数をカウントして起動を制限する。（課題にする？）
			// IpcClientを起動する
			// CLTの起動数が制限に達した場合起動ボタンをdisableにする。

			if (_cCount <= 31) {
				_clt.ShowDialog();
			}
		}

		private void frmIpcController_Load(object sender, EventArgs e) {
			_srv = frmIpcServer.GetInstance();
			_clt = new frmIpcClient();
			this.SettingTabIndex();
		}
	}
}
