﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IpcHandling {

	/// <summary>
	/// Server用フォーム（シングルトン）
	/// </summary>
	public partial class frmIpcServer : Form {

		/// <summary>
		/// シングルトン用のメンバ変数
		/// </summary>
		private static frmIpcServer _formSrv;

		/// <summary>
		/// プロセス間通信のクラス（Server用）
		/// </summary>
		private IpcServer _srv;

		/// <summary>
		/// コンストラクター
		/// </summary>
		private frmIpcServer() {
			InitializeComponent();
		}

		/// <summary>
		/// フォームのタブを決める設定メソッド
		/// </summary>
		private void SettingTabIndex() {
			this.tboxMessage.TabIndex = 10;
			this.btnReflesh.TabIndex = 20;
		}

		/// <summary>
		/// フォームが出力される際に呼び出されるイベント
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmIpcServer_Load(object sender, EventArgs e) {
			_srv = new IpcServer();
			this.SettingTabIndex();
		}

		/// <summary>
		/// クリアボタンが押された時のイベント
		/// 今は、clientが動作するとserverを操作できないため、未完成
		/// 課題：frmServerとfrmClientをモードレスにしてどちらも動かせるようにする。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnReflesh_Click(object sender, EventArgs e) {
			this.tboxMessage.Text = "";
		}

		/// <summary>
		/// frmIpcClientで送信ボタンが押された後に処理されるメソッド
		/// </summary>
		public void GetClientMessage() {
			this.tboxMessage.Text = _srv.RemoteObject.Message;
		}

		/// <summary>
		/// frmIpcServerのインスタンスを取得するメソッド
		/// </summary>
		/// <param name="autoCreate">インスタンスを自動生成するかどうかを決める</param>
		/// <returns></returns>
		public static frmIpcServer GetInstance(bool autoCreate = true){

			// frmIpcServerがnullかつautoCreateがtrueの時に、インスタンスを生成する
			if (_formSrv == null && autoCreate == true) {
				_formSrv = new frmIpcServer();
			}
			return _formSrv;
		}
	}
}
