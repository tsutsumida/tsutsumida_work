﻿namespace IpcHandling {
	partial class frmIpcServer {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.tboxMessage = new System.Windows.Forms.TextBox();
			this.btnReflesh = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// tboxMessage
			// 
			this.tboxMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tboxMessage.Location = new System.Drawing.Point(12, 12);
			this.tboxMessage.Multiline = true;
			this.tboxMessage.Name = "tboxMessage";
			this.tboxMessage.ReadOnly = true;
			this.tboxMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tboxMessage.Size = new System.Drawing.Size(513, 273);
			this.tboxMessage.TabIndex = 0;
			// 
			// btnReflesh
			// 
			this.btnReflesh.Location = new System.Drawing.Point(450, 291);
			this.btnReflesh.Name = "btnReflesh";
			this.btnReflesh.Size = new System.Drawing.Size(75, 23);
			this.btnReflesh.TabIndex = 1;
			this.btnReflesh.Text = "クリア";
			this.btnReflesh.UseVisualStyleBackColor = true;
			this.btnReflesh.Click += new System.EventHandler(this.btnReflesh_Click);
			// 
			// frmIpcServer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(537, 322);
			this.Controls.Add(this.btnReflesh);
			this.Controls.Add(this.tboxMessage);
			this.Name = "frmIpcServer";
			this.Text = "IPCサーバー";
			this.Load += new System.EventHandler(this.frmIpcServer_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox tboxMessage;
		private System.Windows.Forms.Button btnReflesh;
	}
}