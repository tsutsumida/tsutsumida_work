﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IpcHandling {

	//【課題】
	// cltが先に起動し、後からsrvが起動された場合
	// 送信イベントの登録がされていないので、後からでも対応できるようにする。
	// 1. 定期的にsrvが起動していないか見に行く処理を加える
	// 2. srv登録ボタンを用意して、srv起動後に押したらイベントが登録されるようにする。
	// ⇒srv消えた場合は、消える時にイベントも解除するとかにしようかな。

	public partial class frmIpcClient : Form {

		/// <summary>
		/// プロセス間通信を行うクラス（クライアント）
		/// </summary>
		private IpcClient _clt;

		/// <summary>
		/// CLT -> SRVへ通知を行うためのイベント
		/// </summary>
		private event IpcRemoteObject.SendMessageEventHandler handler;

		/// <summary>
		/// コンストラクター
		/// </summary>
		public frmIpcClient() {
			InitializeComponent();
		}

		/// <summary>
		/// TabIndexの設定をする。
		/// </summary>
		private void SettingTabIndex() {
			this.tboxMessage.TabIndex = 10;
			this.btnSend.TabIndex = 20;
		}
		/// <summary>
		/// メッセージをServerへ送信する。
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSend_Click(object sender, EventArgs e) {
			this._clt.RemoteObject.Message = this.tboxMessage.Text;

			//イベント発生させて、frmIpcServerへ情報を渡す。
			//OnSendMessage(new EventArgs());
			//_clt.RemoteObject.SendMessage();
			if (handler != null) {
				handler();
			}
		}

		/// <summary>
		/// frmIpcClientが出力される時に呼ばれるイベント
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmIpcClient_Load(object sender, EventArgs e) {
			_clt = new IpcClient();
			frmIpcServer _srv = frmIpcServer.GetInstance(false);
			if (_srv != null) {
				handler += _srv.GetClientMessage;
			}

			this.SettingTabIndex();
		}
	}
}
