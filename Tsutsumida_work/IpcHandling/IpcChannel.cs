﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;


namespace IpcHandling {

	/// <summary>
	/// プロセス間(IPC)通信をするクラス
	/// Server
	/// </summary>
	class IpcServer {

		/// <summary>
		/// Server/Client間で利用するオブジェクト
		/// </summary>
		public IpcRemoteObject RemoteObject {get;set;}

		/// <summary>
		/// コンストラクター
		/// channel名：IpcServer
		/// url：ipc://IpcServer/test で受け付ける。
		/// Object：IpcRemoteObject
		/// </summary>
		public IpcServer() {
			IpcServerChannel channel = new IpcServerChannel("IpcServer");
			RemoteObject = new IpcRemoteObject();
			RemotingServices.Marshal(RemoteObject,"test",typeof(IpcRemoteObject));
		}
	}

	/// <summary>
	/// プロセス間(IPC)通信を行うクラス
	/// Client
	/// </summary>
	class IpcClient {
		public IpcRemoteObject RemoteObject { get;set;}

		public IpcClient() {
			IpcClientChannel channel = new IpcClientChannel();
			ChannelServices.RegisterChannel(channel,true);
			RemoteObject = Activator.GetObject(typeof(IpcRemoteObject),"ipc://IpcServer/test") as IpcRemoteObject;
		}
	}

	/// <summary>
	/// プロセス間（IPC）通信で情報をやり取りするクラス
	/// </summary>
	class IpcRemoteObject : MarshalByRefObject {
		
		/// <summary>
		/// 
		/// </summary>
		private string _message;

		/// <summary>
		/// ClientからServerへ通知を行うイベント
		/// （メッセージ送信用）
		/// </summary>
		public delegate void SendMessageEventHandler();

		/// <summary>
		/// Client -> Serverへ送信するメッセージを格納
		/// </summary>
		public string Message {
			get { return _message;}
			set { _message = value;}
		}
	}
}
