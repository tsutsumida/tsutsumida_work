﻿namespace XmlFileHandling {
	partial class frmSampleXml {
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent() {
			this.tboxXml = new System.Windows.Forms.TextBox();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.btnSaveXml = new System.Windows.Forms.Button();
			this.tboxValue1 = new System.Windows.Forms.TextBox();
			this.tboxOperation = new System.Windows.Forms.TextBox();
			this.tboxValue2 = new System.Windows.Forms.TextBox();
			this.tboxResult = new System.Windows.Forms.TextBox();
			this.lblEqual = new System.Windows.Forms.Label();
			this.btnCreateXml = new System.Windows.Forms.Button();
			this.btnReadXml = new System.Windows.Forms.Button();
			this.tableLayoutPanelHeader = new System.Windows.Forms.TableLayoutPanel();
			this.panelFooter = new System.Windows.Forms.Panel();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.tableLayoutPanelHeader.SuspendLayout();
			this.panelFooter.SuspendLayout();
			this.SuspendLayout();
			// 
			// tboxXml
			// 
			this.tboxXml.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tboxXml.Location = new System.Drawing.Point(12, 83);
			this.tboxXml.Multiline = true;
			this.tboxXml.Name = "tboxXml";
			this.tboxXml.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tboxXml.Size = new System.Drawing.Size(394, 242);
			this.tboxXml.TabIndex = 1;
			// 
			// btnSaveXml
			// 
			this.btnSaveXml.Location = new System.Drawing.Point(144, 33);
			this.btnSaveXml.Name = "btnSaveXml";
			this.btnSaveXml.Size = new System.Drawing.Size(135, 23);
			this.btnSaveXml.TabIndex = 3;
			this.btnSaveXml.Text = "xmlファイルの保存";
			this.btnSaveXml.UseVisualStyleBackColor = true;
			this.btnSaveXml.Click += new System.EventHandler(this.btnSaveXml_Click);
			// 
			// tboxValue1
			// 
			this.tboxValue1.Location = new System.Drawing.Point(6, 3);
			this.tboxValue1.Name = "tboxValue1";
			this.tboxValue1.ReadOnly = true;
			this.tboxValue1.Size = new System.Drawing.Size(49, 19);
			this.tboxValue1.TabIndex = 4;
			// 
			// tboxOperation
			// 
			this.tboxOperation.Location = new System.Drawing.Point(61, 3);
			this.tboxOperation.Name = "tboxOperation";
			this.tboxOperation.ReadOnly = true;
			this.tboxOperation.Size = new System.Drawing.Size(25, 19);
			this.tboxOperation.TabIndex = 5;
			// 
			// tboxValue2
			// 
			this.tboxValue2.Location = new System.Drawing.Point(92, 3);
			this.tboxValue2.Name = "tboxValue2";
			this.tboxValue2.ReadOnly = true;
			this.tboxValue2.Size = new System.Drawing.Size(49, 19);
			this.tboxValue2.TabIndex = 6;
			// 
			// tboxResult
			// 
			this.tboxResult.Location = new System.Drawing.Point(164, 3);
			this.tboxResult.Name = "tboxResult";
			this.tboxResult.ReadOnly = true;
			this.tboxResult.Size = new System.Drawing.Size(101, 19);
			this.tboxResult.TabIndex = 7;
			// 
			// lblEqual
			// 
			this.lblEqual.AutoSize = true;
			this.lblEqual.Location = new System.Drawing.Point(147, 6);
			this.lblEqual.Name = "lblEqual";
			this.lblEqual.Size = new System.Drawing.Size(11, 12);
			this.lblEqual.TabIndex = 8;
			this.lblEqual.Text = "=";
			// 
			// btnCreateXml
			// 
			this.btnCreateXml.Location = new System.Drawing.Point(3, 3);
			this.btnCreateXml.Name = "btnCreateXml";
			this.btnCreateXml.Size = new System.Drawing.Size(132, 23);
			this.btnCreateXml.TabIndex = 0;
			this.btnCreateXml.Text = "xmlファイルの生成";
			this.btnCreateXml.UseVisualStyleBackColor = true;
			this.btnCreateXml.Click += new System.EventHandler(this.btnCreateXml_Click);
			// 
			// btnReadXml
			// 
			this.btnReadXml.Location = new System.Drawing.Point(3, 33);
			this.btnReadXml.Name = "btnReadXml";
			this.btnReadXml.Size = new System.Drawing.Size(132, 23);
			this.btnReadXml.TabIndex = 2;
			this.btnReadXml.Text = "xmlファイルの読み込み";
			this.btnReadXml.UseVisualStyleBackColor = true;
			this.btnReadXml.Click += new System.EventHandler(this.btnReadXml_Click);
			// 
			// tableLayoutPanelHeader
			// 
			this.tableLayoutPanelHeader.ColumnCount = 2;
			this.tableLayoutPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelHeader.Controls.Add(this.btnSaveXml, 1, 1);
			this.tableLayoutPanelHeader.Controls.Add(this.btnReadXml, 0, 1);
			this.tableLayoutPanelHeader.Controls.Add(this.btnCreateXml, 0, 0);
			this.tableLayoutPanelHeader.Location = new System.Drawing.Point(15, 12);
			this.tableLayoutPanelHeader.Name = "tableLayoutPanelHeader";
			this.tableLayoutPanelHeader.RowCount = 2;
			this.tableLayoutPanelHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanelHeader.Size = new System.Drawing.Size(282, 60);
			this.tableLayoutPanelHeader.TabIndex = 10;
			// 
			// panelFooter
			// 
			this.panelFooter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.panelFooter.Controls.Add(this.tboxResult);
			this.panelFooter.Controls.Add(this.lblEqual);
			this.panelFooter.Controls.Add(this.tboxValue2);
			this.panelFooter.Controls.Add(this.tboxOperation);
			this.panelFooter.Controls.Add(this.tboxValue1);
			this.panelFooter.Location = new System.Drawing.Point(12, 339);
			this.panelFooter.Name = "panelFooter";
			this.panelFooter.Size = new System.Drawing.Size(392, 24);
			this.panelFooter.TabIndex = 11;
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			// 
			// frmSampleXml
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(419, 376);
			this.Controls.Add(this.panelFooter);
			this.Controls.Add(this.tableLayoutPanelHeader);
			this.Controls.Add(this.tboxXml);
			this.Name = "frmSampleXml";
			this.Text = "frmSampleXml";
			this.Load += new System.EventHandler(this.frmSampleXml_Load);
			this.tableLayoutPanelHeader.ResumeLayout(false);
			this.panelFooter.ResumeLayout(false);
			this.panelFooter.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox tboxXml;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.Button btnSaveXml;
		private System.Windows.Forms.TextBox tboxValue1;
		private System.Windows.Forms.TextBox tboxOperation;
		private System.Windows.Forms.TextBox tboxValue2;
		private System.Windows.Forms.TextBox tboxResult;
		private System.Windows.Forms.Label lblEqual;
		private System.Windows.Forms.Button btnCreateXml;
		private System.Windows.Forms.Button btnReadXml;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanelHeader;
		private System.Windows.Forms.Panel panelFooter;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
	}
}

