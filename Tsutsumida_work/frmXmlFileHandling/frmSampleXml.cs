﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace XmlFileHandling {
	public partial class frmSampleXml : Form {
		private SampleXml _xml;
		private string _filepath;
		public frmSampleXml() {
			InitializeComponent();
		}

		/// <summary>
		/// フォームボタンのタブの順番を設定する。
		/// </summary>
		private void SettingTabIndex(){
			// header部インデックス　0～100番
			this.tableLayoutPanelHeader.TabIndex = 5; //追記
			this.btnCreateXml.TabIndex = 10;
			this.btnReadXml.TabIndex = 20;
			this.btnSaveXml.TabIndex = 30;

			// body部インデックス100～200番台
			this.tboxXml.TabIndex = 110;

			// footer部インデックス　200～300番台
			this.panelFooter.TabIndex = 205;
			this.tboxValue1.TabIndex = 210;
			this.tboxOperation.TabIndex = 220;
			this.tboxValue2.TabIndex = 230;
			this.tboxResult.TabIndex = 240;
		}

		/// <summary>
		/// xmlファイルの生成ボタンが押された時のイベント
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnCreateXml_Click(object sender, EventArgs e) {
			this.saveFileDialog1.Filter = "xml files (*.xml)|*.xml| All files (*.*)|*.*";
			if (this.saveFileDialog1.ShowDialog() != DialogResult.OK) {
				return ;
			}

			_filepath = this.saveFileDialog1.FileName;
			/*
			string data = _xml.CreateSampleXml();
			Encoding enc = Encoding.GetEncoding("shift-jis");
			using (StreamWriter sr = new StreamWriter(_filepath, false, enc)) {
				sr.Write(data);
			}
			 */
			_xml.CreateSampleXml(_filepath);
			_filepath = "";
		}

		/// <summary>
		/// xmlファイルの読み込みボタンが押された時のイベント
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnReadXml_Click(object sender, EventArgs e) {
			this.openFileDialog1.Filter = "xml files (*.xml)|*.xml| All files (*.*)|*.*";
			if(this.openFileDialog1.ShowDialog() != DialogResult.OK){
				return ;
			}

			_filepath = openFileDialog1.FileName;
			using(Stream stream = openFileDialog1.OpenFile()){
				using (StreamReader sr = new StreamReader(stream)) {
					try {
						string data = sr.ReadToEnd();
						this.tboxXml.Text = data;

					} catch (Exception ex) {
					}
				}
				_xml = _xml.LoadSampleXml(_filepath);
				this.SetSampleXmlData();
				_filepath = "";
			}
		}

		/// <summary>
		/// xmlファイルの保存ボタンが押された時のイベント
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSaveXml_Click(object sender, EventArgs e) {
			this.saveFileDialog1.Filter = "xml files (*.xml)|*.xml| All files (*.*)|*.*";
			if (this.saveFileDialog1.ShowDialog() != DialogResult.OK) {
				return ;
			}
			_filepath = this.saveFileDialog1.FileName;
			string data = this.tboxXml.Text;
			Encoding enc = Encoding.GetEncoding("shift-jis");
			using (StreamWriter sr = new StreamWriter(_filepath, false,enc)){
				sr.Write(data);
			}

			// 課題:書き込みが完了するのを待つ処理を入れる。
			_xml = _xml.LoadSampleXml(_filepath);
			this.SetSampleXmlData();
			_filepath = "";
		}

		/// <summary>
		/// SampleXml.xmlから取得してきた情報を画面に反映する
		/// </summary>
		private void SetSampleXmlData() {
			int value1 = this._xml.cCalculate.value1;
			int value2 = this._xml.cCalculate.value2;
			string operation = this._xml.cCalculate.operation;

			int r = 0;
			switch (operation) {
				//case '+': r = value1 + value2; break;
				//case '-': r = value1 - value2; break;
				//case '*': r = value1 * value2; break;
				//case '/': r = value1 / value2; break;
				case "+": r = value1 + value2; break;
				case "-": r = value1 - value2; break;
				case "*": r = value1 * value2; break;
				case "/": r = value1 / value2; break;
				default: r = 0; break;
			}

			this.tboxValue1.Text = value1.ToString();
			this.tboxOperation.Text = operation.ToString();
			this.tboxValue2.Text = value2.ToString();
			this.tboxResult.Text = r.ToString();
		}

		/// <summary>
		/// 表示される際に呼び出されるイベント
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void frmSampleXml_Load(object sender, EventArgs e) {
			_xml = new SampleXml();
			SettingTabIndex();
		}
	}
}
