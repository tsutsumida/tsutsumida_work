﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace XmlFileHandling {

	/// <summary>
	/// frmSampleXmlで出力するxmlファイルを扱うクラス
	/// </summary>
	public class SampleXml {
		//private XmlSerializer serializer(Type.GetType("SampleXml"));
		public Calculate cCalculate = new Calculate();
		
		/// <summary>
		/// SampleXmlクラスからxmlファイルを作成する
		/// </summary>
		/// <returns></returns>
		public bool CreateSampleXml(string filepath) {
			bool result = false;
			try {
				XmlSerializer serializer = new XmlSerializer(typeof(SampleXml));
				SampleXml sxml = new SampleXml();

				Encoding enc = Encoding.GetEncoding("shift-jis");
				using (StreamWriter sr = new StreamWriter(filepath, false, enc)) {
					serializer.Serialize(sr, sxml);
					sr.Close();
				}
			} catch (Exception e) {

			}
			return result;
		}

		/// <summary>
		/// xmlファイルを読み込み、パラメータにセットする
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public SampleXml LoadSampleXml(string filepath) {
			SampleXml sxml = new SampleXml();
			try {
				XmlSerializer serializer = new XmlSerializer(typeof(SampleXml));
				//Stream stream = new MemoryStream(Encoding.Unicode.GetBytes(data));
				using(Stream stream = File.Open(filepath,FileMode.Open)){
					sxml = (SampleXml)serializer.Deserialize(stream);
					stream.Close();
				}
			} catch (Exception e) {
			}
			return sxml;
		}

		/// <summary>
		/// xmlのタグ
		/// 計算タグ
		/// </summary>
		public class Calculate {
			/// <summary>
			/// 数値1
			/// </summary>
			public int value1 = 0;

			/// <summary>
			/// 四則演算（+,-,*,/)
			/// </summary>
			public string operation = "+";
			
			/// <summary>
			/// 数値2
			/// </summary>
			public int value2 = 0;
		}
	}
}
